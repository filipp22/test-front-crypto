import { API_KEYS, TSYMS } from "../Config";

const DetailedCoinAPI = async (coin = "") => {
  const response = await fetch(
    `https://min-api.cryptocompare.com/data/v2/histoday?fsym=${coin}&tsym=${TSYMS}&limit=30&api_key=${API_KEYS}`
  );
  const json = await response.json();

  return json;
};

export default DetailedCoinAPI;
