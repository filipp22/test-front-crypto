import { API_KEYS, COINS, TSYMS } from "../Config";

const SymbolsPriceAPI = async () => {

  const response = await fetch(
    `https://min-api.cryptocompare.com/data/pricemulti?fsyms=${COINS.join(
      ","
    )}&tsyms=${TSYMS}&api_key=${API_KEYS}`
  );
  const json = await response.json();

  return json;

};

export default SymbolsPriceAPI;
