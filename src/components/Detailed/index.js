import React, { useEffect, useState, useCallback } from "react";
import PropTypes from "prop-types";
import "./styles.css";
import { connect } from "react-redux";
import DetailedCoinAPI from "../../api/DetailedCoinAPI";
import { COINS } from "../../Config";

function Detailed({ coins, match }) {
  const { coin } = match?.params;

  const [getCoins] = useState(COINS);

  const [getDetailed, setDetailed] = useState(undefined);

  const fetchCoin = useCallback(() => {
 
    if (getCoins.includes(coin)) {
      DetailedCoinAPI(coin).then((res) => {
        setDetailed(res?.Data?.Data);
      });
    }
  }, [coin, getCoins]);

  useEffect(() => fetchCoin(), [fetchCoin]);

  return <>{JSON.stringify(getDetailed)}</>;
}

Detailed.propTypes = {
  coins: PropTypes.object.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      coin: PropTypes.string.isRequired,
    }),
  }),
};

const mapStateToProps = (state) => ({
  coins: state?.coins,
});

export default connect(mapStateToProps)(Detailed);
