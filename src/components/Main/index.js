import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import "./styles.css";
import { connect } from "react-redux";
import { updateAll } from "../../store/actions/coins";
import SymbolsPriceAPI from "../../api/SymbolsPriceAPI";
import { COINS_UPDATE_TIME, TSYMS } from "../../Config";
import Modal from 'react-modal';
import Close from '../Icon/Close'
import Chart from './Chart'


Modal.setAppElement('#root');

function Main({ coins, dispatch }) {

  const [modalIsOpen, setIsOpen] = useState(false);


  const priceFormat = (n) => {
    return typeof n !== undefined ? parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").replace('.', ',') : 0;
  }

  const closeModal = () => {
    setIsOpen(false)
  }


  useEffect(() => {
    SymbolsPriceAPI().then((res) => dispatch(updateAll(res)));
    setInterval(
      () => SymbolsPriceAPI().then((res) => dispatch(updateAll(res))),
      COINS_UPDATE_TIME * 1000
    );

  }, []);

  return <>
    <div className="container">
      <h1 className="title">Валюты</h1>
      <div className="list-coins">
        {Object.keys(coins).map((item, index) => {
          return (<div key={index} className="item">
            <div className="item-content">
              <div className="name">{item} <span>/ {TSYMS}</span></div>
              <div className="price">{priceFormat(Object.values(coins[item]))}</div>
              <a className="link-charts" onClick={() => setIsOpen(item)} >Смотреть график</a>
            </div>
          </div>)
        })}
      </div>
    </div>

    <Modal
      isOpen={Boolean(modalIsOpen)}
      onRequestClose={closeModal}
      contentLabel=""
      className="Modal"
      overlayClassName="modal-overlay"
    >
      <div className="modal-body ">
        <Close className={"modal-close"} action={closeModal} />
        <div className="modal-charts">
          <div className="modal-title">Динамика курса валюты - {modalIsOpen} <span>/ USDT</span></div>
          <Chart coin={modalIsOpen}/>
        </div>

      </div>


    </Modal>
  </>
    ;
}

Main.propTypes = {
  dispatch: PropTypes.func.isRequired,
  coins: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  coins: state?.coins,
});

export default connect(mapStateToProps)(Main);
