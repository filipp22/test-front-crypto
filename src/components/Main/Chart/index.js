import React, { useState, useCallback, useEffect } from 'react'
import PropTypes, { bool } from "prop-types";
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import DetailedCoinAPI from "../../../api/DetailedCoinAPI";



function Chart({ coin }) {

    const [getDetailed, setDetailed] = useState([]);

    const dataFormated = (data) => {
        let newObj = []
        data.map((item, index) => {
            newObj.push([item?.time * 1000, item?.high])
        })

        return newObj
    }



    const options = {
        title: {
            text: ''
        },
        series: [{
            data: dataFormated(getDetailed),
            name: coin
        }],
        xAxis: {
            type: 'datetime',
            labels: {
                format: '{value:%Y-%m-%d}',
            }
        },
    }


    const fetchCoin = useCallback(() => {
        DetailedCoinAPI(coin).then((res) => {
            setDetailed(res?.Data?.Data);
        });

    }, [coin]);

    useEffect(() => fetchCoin(), []);

    if (typeof coin === bool)
        return <></>

    return <div className="charts-main">
        <HighchartsReact
            highcharts={Highcharts}
            options={options}
        />
    </div>
}



Chart.propTypes = {
    coin: PropTypes.any
};


export default Chart