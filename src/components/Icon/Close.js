import React from 'react'
import PropTypes from "prop-types";

function Close({action = ()=> {}, fill = "#000", className, size = 20 }) {
    return <svg onClick={action} viewBox={`0 0 19 19`} className={className}>
        <path
            fill={fill}
            d="m13.77812,2.30631l-4.77995,4.77993l4.77995,4.77995c0.5258,0.5258 0.5258,1.38611 0,1.91191c-0.5258,0.5258 -1.38611,0.5258 -1.91191,0l-4.77993,-4.77995l-4.77993,4.77995c-0.5258,0.5258 -1.38617,0.5258 -1.91197,0c-0.52578,-0.5258 -0.52578,-1.38611 0,-1.91191l4.77993,-4.77995l-4.77993,-4.77993c-0.5258,-0.5258 -0.5258,-1.38617 0,-1.91197c0.52578,-0.52578 1.38617,-0.52578 1.91197,0l4.77993,4.77993l4.77993,-4.77993c0.5258,-0.52578 1.38611,-0.52578 1.91191,0c0.5258,0.5258 0.5258,1.38617 0,1.91197z"
        />
    </svg>
}



Close.propTypes = {
    size: PropTypes.string,
    fill: PropTypes.string,
    className: PropTypes.string
};


export default Close