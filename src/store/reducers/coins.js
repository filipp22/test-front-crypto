import { COINS } from "../../Config";

const LOAD_COINS = "LOAD_COINS";

const initialState = {
  ...COINS,
};

const coins = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_COINS: {
      const { payload } = action;
      if (payload) {
        return payload;
      }
      return state;
    }
    default:
      return state;
  }
};

export default coins;
