const LOAD_COINS = "LOAD_COINS"

export const updateAll = (params) => {
  return { type: LOAD_COINS, payload: params };
}
