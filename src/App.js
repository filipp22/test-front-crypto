import React, { Component } from "react";
import { Route, Switch, BrowserRouter, withRouter } from "react-router-dom";
import "./assets/scss/reset.sass"
import "./assets/scss/app.sass";
import "./assets/scss/modal.sass";
import Main from "./components/Main";
import Detailed from "./components/Detailed";
import 'react-router-modal/css/react-router-modal.css'



class App extends Component {
  render() {
    const { history } = this.props;

    return (
      <BrowserRouter>
        <Switch>
          <Route exact history={history} path="/" component={Main} />
          {/* <Route exact history={history} path="/details/:coin" component={Detailed} /> */}
          <Route>{'404'}</Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default withRouter(App);
